#!/bin/sh
# autopkgtest check based on upstream longcheck check ($ make longcheck)
# written for Debian by Jerome Benoit <calculus@rezozer.net> on behalf of the Debian Science Team
# copyright: 2015-2016 Jerome Benoit <calculus@rezozer.net>
# distributed under the terms and conditions of GPL version 3 or later

# comments:
#  bugreport #804408: constant failure of the make-longcheck autopkgtest test:
#   1] valgrind check is temporarily neutralized;
#   2] a bugreport has been submit for allowing long autopkgtest tests, #804406 .
#   -- Jerome Benoit <calculus@rezozer.net>  Sun, 08 Nov 2015

set -ue

cat << EOF > $AUTOPKGTEST_TMP/Makefile

abs_top_pkgdir = $PWD

GMPECM = /usr/bin/ecm


LISTOFMETHODS = pp1 pm1 ecm
LISTOFVARIANTS = + -no-ntt -modmuln -redc -mpzmod

export GMPECM_DATADIR=\$(abs_top_pkgdir)

default:

longcheck: \$(foreach _m,\$(LISTOFMETHODS),\$(foreach _v,\$(LISTOFVARIANTS),longcheck+test+\$(_m)+\$(_v))) \$(foreach _m,\$(LISTOFMETHODS),longcheck+testlong+\$(_m)) longcheck+testtree+ecm
	@

longcheck+test+%:
	@mkdir \$@
	( cd \$@ ; \$(abs_top_pkgdir)/test.\$(word 3,\$(subst +, ,\$@)) "\$(GMPECM) \$(word 4,\$(subst +, ,\$@))" ;)

longcheck+testlong+%:
	@mkdir \$@
	( cd \$@ ; \$(abs_top_pkgdir)/testlong.\$(word 3,\$(subst +, ,\$@)) "\$(GMPECM)" ;)

longcheck+testtree+ecm:
	@mkdir \$@
	( cd \$@ ; \$(abs_top_pkgdir)/test.\$(word 3,\$(subst +, ,\$@)) "\$(GMPECM) -treefile tree" ;)

EOF

NJOBS=$(getconf _NPROCESSORS_ONLN)

LLAVG=$(( $NJOBS * 175 ))
LLAVG=$(( $LLAVG / 100 ))

make -C $AUTOPKGTEST_TMP -k -j $NJOBS -l $LLAVG longcheck

exit 0
